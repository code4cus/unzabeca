@extends('layouts.webview')

@section('title', 'Executive')

@section('content')

    <section class="page-header-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <h1>Executive</h1>
                </div>
                <ol class="breadcrumb">
                    <li><a href="/">Home</a></li>
                    <li><a href="#">About</a></li>
                    <li class="active">Executive</li>
                </ol>
            </div>
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
    </section>


    <div class="container-fluid">
        <div class="content-wrapper">
            <section class="team-wrapper">
                <h2 class="section-title">Our Team</h2>
                <p style="font-size: 15px">Meet our great team of warm people ready to help you in anyway.</p>
                <div class="css-team">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-lg-12">
                            @foreach($executives as $executive)
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    <figure>
                                        <div class="css-team-avatar" style="-webkit-border-radius: 300px;-moz-border-radius: 300px;border-radius: 300px">
                                            <div class="css-overlay"></div>
                                            @if(isset($executive->photo))
                                                <img class="media-object img-responsive" src="{{URL::asset('../storage/'.$executive->photo)}}" alt="Image" style="border-radius: 50%">
                                            @endif
                                            <a class="readmore" href="#"><i class="flaticon-square57"></i></a>
                                        </div>
                                        <div class="css-team-info">
                                            <h3 class="css-team-heading">{{ $executive->first_name}} {{$executive->middle_name }} {{$executive->last_name }}<br> <small>{{$executive->position->position_description }}</small></h3>
                                        </div>
                                    </figure>
                                </div><!-- /.col-md-4 -->
                            @endforeach

                        </div>
                        <!-- /.col-md-12 -->
                    </div>>
                    <!-- /.row -->
                </div><!-- /.css-team -->
            </section>
        </div><!-- /.content-wrapper -->
    </div><!-- /.container-fluid -->

@endsection