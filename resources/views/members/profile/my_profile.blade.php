@extends('layouts.members')
@section('title', 'My profile')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">{{$user->member->first_name}}</div>

                <div class="panel-body">
                    <div class="col-md-9">
                        <form class="form-horizontal" role="form" style="margin-left: 20px;margin-top: 20px">
                            <div class="form-group">
                                <label class="col-sm-3 col-md-3 col-xs-4" for="first-name">First Name:</label>
                                <div class="col-sm-6 col-md-6 col-xs-5">
                                    <label class="text-primary"
                                           for="first_name_value">{{$user->member->first_name}}</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 col-md-3 col-xs-4" for="last-name"> Middle Name:</label>
                                <div class="col-sm-6 col-md-6 col-xs-5">
                                    <label class="text-primary"
                                           for="last_name_value">{{$user->member->middle_name}}</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 col-md-3 col-xs-4" for="last-name"> Last Name:</label>
                                <div class="col-sm-6 col-md-6 col-xs-5">
                                    <label class="text-primary"
                                           for="last_name_value">{{$user->member->last_name}}</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 col-md-3 col-xs-4" for="e-mail"> Email Address:</label>
                                <div class="col-sm-6 col-md-6 col-xs-5">
                                    <label class="text-primary" for="e-mail_value">{{$user->email}}</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 col-md-3 col-xs-4" for="man-number"> Member id:</label>
                                <div class="col-sm-6 col-md-6 col-xs-5">
                                    <label class="text-primary"
                                           for="man-number_value">{{$user->member->member_id}}</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 col-md-3 col-xs-4" for="status_field">Status</label>
                                <div class="col-sm-6 col-md-6 col-xs-5">
                                    <label class="text-primary"
                                           for="status_field_label">{{$user->member->status->status_description}}</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 col-md-3 col-xs-4" for="residential-address"> Position</label>
                                <div class="col-sm-6 col-md-6 col-xs-5">
                                    <label class="text-primary"
                                           for="residential_address_value">{{$user->member->position->position_description}}</label>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-3 col-md-3 col-xs-4" for="contract-expiry-date">Year</label>
                                <div class="col-sm-6 col-md-6 col-xs-5">
                                    <label class="text-primary"
                                           for="contract_expiry_date_value">{{\Carbon\Carbon::parse($user->member->year)->toFormattedDateString()}}</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-8 col-md-9 col-xs-6">
                                    <a href="{{url('/members/my_profile/edit')}}" class="btn btn-info btn-sm"
                                       role="button">Edit profile</a>
                                </div>
                            </div>

                        </form>
                        <!-- /.form -->
                    </div>

                    <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                        <a href="#" data-toggle="modal" data-target="#avatar-modal-{{$user->member->member_id}}">
                            <div class="css-team-avatar"
                                 style="-webkit-border-radius: 300px;-moz-border-radius: 300px;border-radius: 300px">
                                <div class="css-overlay"></div>
                                <img class="media-object img-responsive"
                                     src="{{URL::asset('../storage/'.$user->member->photo)}}" alt="Image"
                                     style="border-radius: 50%">
                                <a class="readmore" href="#"><i class="flaticon-square57"></i></a>
                            </div>
                        </a>

                        <div class="profile_img">

                            <!-- end of image cropping -->

                            <!-- Cropping modal -->
                            <div class="modal fade" id="avatar-modal-{{$user->member->member_id}}" aria-hidden="true"
                                 aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <form class="avatar-form" action="{{url('members/changePicture/'.$user->member->member_id)}}" enctype="multipart/form-data" method="post">
                                            {{csrf_field()}}
                                            <div class="modal-header">
                                                <button class="close" data-dismiss="modal" type="button">&times;
                                                </button>
                                                <h4 class="modal-title" id="avatar-modal-label">Change Avatar</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="avatar-body">

                                                    <!-- Upload image and data -->
                                                    <div class="avatar-upload">
                                                        <input class="avatar-src" name="avatar_src" type="hidden">
                                                        <input class="avatar-data" name="avatar_data" type="hidden">
                                                        <label for="avatarInput">Local upload</label>
                                                        <input class="form-control" id="photo" name="photo" type="file">
                                                        @if ($errors->has('photo'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('photo') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="modal-footer">
                                                    <button type="submit" class="btn btn-default btn-primary col-lg-offset-9 col-md-offset-9 col-sm-offset-9 col-xs-offset-7">Upload</button>
                                                    <button type="reset" class="btn btn-default btn-danger pull-right" data-dismiss="modal">Cancel</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- /.modal -->

                        </div>

                    </div>

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->



@endsection
